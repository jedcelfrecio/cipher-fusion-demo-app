## Steps To Run

1. Make sure that the DB_CONNECTION, DB_HOST, DB_PORT, DB_USERNAME, and DB_PASSWORD in the .env file are according to your database server. DB_CONNECTION should be "mysql".

2. Make sure MySQL drivers are enabled in your php.ini file.

3. Run "php artisan migrate" and allow it to create a "laravel" database.

4. Run two consoles inside the root directory (cipher-fusion-demo-app). For the first console, run "npm run dev". For the second console, run "php artisan serve".

5. Try out the demo app using the port displayed in the console.

NOTE: The table used for database functions is created the first time you load the main page if it does not exist.