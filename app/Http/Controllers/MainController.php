<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MainController extends Controller
{

    public function render()
    {

        $this->initializeTasksTable();
        return Inertia::render('Main/Show');

    }

    public function initializeTasksTable() {

        if (!Schema::hasTable('tasks')) {
            Schema::create('tasks', function(Blueprint $table) {
                $table->id();
                $table->string('title');
                $table->string('description');
            });
        }

    }

    public function createTask(Request $request) {

        DB::table('tasks')->insert([
            'title' => $request->title,
            'description' => $request->description
        ]);

    }

    public function getTasks() {
        
        $tasks = DB::table('tasks')->get();
        return $tasks;

    }

    public function editTask(Request $request) {

        DB::table('tasks')
          ->where('id', $request->id)
          ->update(['title' => $request->title, 'description' => $request->description]);

    }

    public function deleteTask(Request $request) {
        DB::table('tasks')->where('id', $request->id)->delete();
    }

}