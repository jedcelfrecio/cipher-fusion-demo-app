<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\MainController;

Route::get('/', [MainController::class, 'render']);

Route::post('/create-task', [MainController::class, 'createTask']);

Route::get('/get-tasks', [MainController::class, 'getTasks']);

Route::post('/edit-task', [MainController::class, 'editTask']);

Route::post('/delete-task', [MainController::class, 'deleteTask']);